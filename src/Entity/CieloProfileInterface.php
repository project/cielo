<?php

namespace Drupal\cielo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cielo profile entities.
 */
interface CieloProfileInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

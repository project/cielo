<?php

namespace Drupal\cielo\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cielo credit card payment entities.
 *
 * @ingroup cielo
 */
class CieloPaymentDeleteForm extends ContentEntityDeleteForm {


}
